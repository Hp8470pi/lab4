﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Optimization;

namespace GameStore.WebUI.App_Start
{
    public static class BundleConfig
    {
        public static void RegisterBundles(BundleCollection bundles)
        {
            //Bootstrap
            bundles.Add(new StyleBundle("~/bundles/bootstrap/css").Include(
                                 "~/Content/bootstrap.min.css", new CssRewriteUrlTransform()));


            bundles.Add(new StyleBundle("~/bundles/Global/css").Include(
                                 "~/Content/Global.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/bundles/Slaider/css").Include(
                              "~/Content/Slaider.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/bundles/Block/css").Include(
                              "~/Content/Block.css", new CssRewriteUrlTransform()));
        

            bundles.Add(new StyleBundle("~/bundles/ErrorStyles/css").Include(
                             "~/Content/ErrorStyles.css", new CssRewriteUrlTransform()));

            bundles.Add(new StyleBundle("~/bundles/bootstrap-theme/css").Include(
                            "~/Content/bootstrap-theme.css", new CssRewriteUrlTransform()));



            //Bootstrap
            bundles.Add(new ScriptBundle("~/bundles/bootstrap/js").Include(
                     "~/Scripts/bootstrap.min.js"));
            bundles.Add(new ScriptBundle("~/bundles/popper.min/js").Include(
                     "~/Scripts/popper.min.js"));

            // jQuery
            bundles.Add(new ScriptBundle("~/bundles/jquery/js").Include(
                      "~/Scripts/jquery-3.4.1.min.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery.validate/js").Include(
                     "~/Scripts/jquery.validate.js"));

            bundles.Add(new ScriptBundle("~/bundles/jquery.validate.unobtrusive/js").Include(
                    "~/Scripts/jquery.validate.unobtrusive.js"));

        }
    }
}